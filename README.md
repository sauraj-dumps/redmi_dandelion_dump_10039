## dandelion-user 10 QP1A.190711.020 V12.5.1.0.QCDCNXM release-keys
- Manufacturer: xiaomi
- Platform: mt6765
- Codename: dandelion
- Brand: Redmi
- Flavor: dandelion-user
- Release Version: 10
- Id: QP1A.190711.020
- Incremental: V12.5.1.0.QCDCNXM
- Tags: release-keys
- CPU Abilist: armeabi-v7a,armeabi
- A/B Device: false
- Locale: zh-CN
- Screen Density: 440
- Fingerprint: Redmi/dandelion/dandelion:10/QP1A.190711.020/V12.5.1.0.QCDCNXM:user/release-keys
- OTA version: 
- Branch: dandelion-user-10-QP1A.190711.020-V12.5.1.0.QCDCNXM-release-keys
- Repo: redmi_dandelion_dump_10039


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
